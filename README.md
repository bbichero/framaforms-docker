Framaforms with docker
------

Requirements:
```
Docker version 18.09.6
docker-compose version 1.24.0
Docker-machine (only on other distributions than Linux)
```

Before build compose file you must edit the same password at 2 places:
In `docker-compose.yml`:
```
@@ -32,7 +32,7 @@
     ports:
       - 5432:5432
     environment:
-      POSTGRES_PASSWORD: frama
+      POSTGRES_PASSWORD: <CHANGE-HERE>
       POSTGRES_USER: framaforms_user
       POSTGRES_DB: framaforms
     volumes:
```

In `phpfpm/settings.php`:

```
@@ -216,7 +216,7 @@
         'driver' => 'pgsql',
         'database' => 'framaforms',
         'username' => 'framaforms_user',
-        'password' => 'frama',
+        'password' => '<CHANGE-HERE>',
         'host' => 'db',
         'port' => '5432',
         'prefix' => '',
```

*Only change password and not username or database name, framaforms need them for database initialisation*

You can now execute:
```
docker-compose build
```

You must check all thoses points before put your instance in production:
- Visit: `http://framaforms.domain.name` and connect with username `admin` and password `changemeASAP!`
- Edit admin password
- Edit informations of your site `http://framaforms.domain.name/admin/config/system/site-information`
- Check paths `http://framaforms.domain.name/admin/config/media/file-system`
- Check your installation and if your modules are up to date `http://framaforms.domain.name/admin/reports/status`
- Optionally, activate cache informations for production use `http://framaforms.domain.name/admin/config/development/performance`
- Enable https, you must edit nginx conf at [nginx/default.conf](nginx/default.conf), and follow [steps](https://devsidestory.com/lets-encrypt-with-docker/) (this will be add in next commits)